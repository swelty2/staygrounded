# StayGrounded

Interactive application where users can interact with data related to coffee/tea, place an order using a customized Amazon Lex Bot, and create their own brew using an interactive jQuery form and p5 coffee cup.

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
